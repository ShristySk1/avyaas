package com.ayata.avyaas.ui

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.ayata.avyaas.MainActivity
import com.ayata.avyaas.R
import kotlinx.android.synthetic.main.fragment_splash.*


class SplashFragment : Fragment(R.layout.fragment_splash) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as MainActivity).setfullScreenMode()
        super.onViewCreated(view, savedInstanceState)
        val animation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        iv_trademark.animation = animation
        Handler(Looper.getMainLooper()).postDelayed({
            run {
                if (onBoardingFinish()) findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
                else findNavController().navigate(R.id.action_splashFragment_to_viewPagerFragment)
            }
        }, 3000)

    }

    private fun onBoardingFinish(): Boolean {
        val sharedPref = requireActivity().getSharedPreferences("onBoarding", Context.MODE_PRIVATE)
        return sharedPref.getBoolean("Finished", false)
    }
}