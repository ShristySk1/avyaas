package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Course
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_course.view.*
import kotlinx.android.synthetic.main.item_on_boarding.view.tv_title

class CourseAdapter(private val courses: List<Course>) :
    RecyclerView.Adapter<CourseAdapter.CourseViewHolder>() {
    inner class CourseViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: Course) {
            itemView.apply {
                tv_title.text = item.title
                tv_description.text = item.description
                tv_subject.text = item.subject
                Glide.with(itemView).load(item.image).into(iv_courseImage)
                itemView.setOnClickListener {
                    itemCourseClick?.let {
                        it(item)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseViewHolder {
        return CourseViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_course, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CourseViewHolder, position: Int) {
        val item = courses[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return courses.size
    }

    private var itemCourseClick: ((Course) -> Unit)? = null
    fun setCourseClickListener(listener: ((Course) -> Unit)) {
        itemCourseClick = listener
    }
}