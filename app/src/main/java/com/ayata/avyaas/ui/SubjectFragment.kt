package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Subject
import com.ayata.avyaas.ui.adpater.SubjectAdapter
import kotlinx.android.synthetic.main.fragment_subject.*


class SubjectFragment : Fragment(R.layout.fragment_subject) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val navHostFragment = (parentFragment as NavHostFragment)
        val parent = (navHostFragment.parentFragment) as MainFragment
        parent.toolbarHeader("Practice")
        super.onViewCreated(view, savedInstanceState)
        val subjects = listOf(
            Subject(
                R.drawable.ic_physics,
                "Physics",
                null
            ),
            Subject(
                R.drawable.ic_chemistry,
                "Chemistry",
                null
            ),
            Subject(
                R.drawable.ic_botany,
                "Botany",
                null
            ),
            Subject(
                R.drawable.ic_zoology,
                "Zoology",
                null
            ),
            Subject(
                R.drawable.ic_general_knowledge,
                "General Knowledge",
                null
            ),
            Subject(
                R.drawable.ic_math,
                "Math",
                null
            ),
            Subject(
                R.drawable.ic_english,
                "English",
                null
            )

        )
        rv_subjects.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 3)
            adapter = SubjectAdapter(subjects = subjects)
        }
    }

}