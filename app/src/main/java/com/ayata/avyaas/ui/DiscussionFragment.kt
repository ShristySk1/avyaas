package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.ayata.avyaas.R
import com.ayata.avyaas.ui.adpater.DiscussionAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_discussion.*


class DiscussionFragment : Fragment(R.layout.fragment_discussion) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navHostFragment = (parentFragment as NavHostFragment)
        val parent = (navHostFragment.parentFragment) as MainFragment
        parent.toolbarHeader("Discusion")
        //fragments
        val discussionAdapter = DiscussionAdapter(
            fragmentManager = childFragmentManager,
            lifecycle,
            3
        )
        viewPager.setAdapter(discussionAdapter)
        val testTypes = listOf(
            "Explore",
            "Recent Discussion",
            "Recent poll"
        )
        //disable swiping
        viewPager.setUserInputEnabled(false);
        TabLayoutMediator(
            tabLayout, viewPager
        ) { tab: TabLayout.Tab?, position: Int -> run { tab?.text = testTypes[position] } }.attach()
    }
}