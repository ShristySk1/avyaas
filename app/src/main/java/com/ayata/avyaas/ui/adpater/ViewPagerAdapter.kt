package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.SplashImage
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_on_boarding.view.*

class ViewPagerAdapter(private val onBoardings: List<SplashImage>) :
    RecyclerView.Adapter<ViewPagerAdapter.OnBoardingViewHolder>() {
    inner class OnBoardingViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: SplashImage) {
            itemView.apply {
                tv_title.text = item.title
                tv_subTitle.text = item.subTitle
                Glide.with(itemView).load(item.image).into(iv_onboardingImage)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnBoardingViewHolder {
        return OnBoardingViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_on_boarding, parent, false)
        )
    }

    override fun onBindViewHolder(holder: OnBoardingViewHolder, position: Int) {
        val item = onBoardings[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return onBoardings.size
    }
}