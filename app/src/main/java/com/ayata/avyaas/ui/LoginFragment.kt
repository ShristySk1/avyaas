package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.ayata.avyaas.R


class LoginFragment : Fragment(R.layout.fragment_login) {
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    val navHostFragment= (parentFragment as NavHostFragment)
    val parent=(navHostFragment.parentFragment) as MainFragment
    parent.toolbarHeader("Login")
  }
  }