package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Offer
import kotlinx.android.synthetic.main.item_offer.view.*
import kotlinx.android.synthetic.main.item_on_boarding.view.tv_title

class ExploreAdapter1(private val offers: List<String>) :
    RecyclerView.Adapter<ExploreAdapter1.OfferViewHolder>() {
    inner class OfferViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: String) {
            itemView.apply {

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {
        return OfferViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_explore1, parent, false)
        )
    }

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) {
        val item = offers[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return offers.size
    }

//    private var itemOfferClick: ((Int) -> Unit)? = null
//    fun setOfferClickListener(listener: ((Int) -> Unit)) {
//        itemOfferClick = listener
//    }
}