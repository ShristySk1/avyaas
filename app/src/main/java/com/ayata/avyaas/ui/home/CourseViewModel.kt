package com.ayata.avyaas.ui.home

import androidx.lifecycle.ViewModel
import com.ayata.avyaas.data.repository.CourseRepository

class CourseViewModel(val repository: CourseRepository) : ViewModel() {
    fun getCourse() =
        repository.getCourses()

}