package com.ayata.avyaas.ui

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.SplashImage
import com.ayata.avyaas.ui.adpater.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_view_pager.*

class ViewPagerFragment : Fragment(R.layout.fragment_view_pager) {
    private val TAG = "ViewPagerFragment"
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val onBoardings = listOf(
            SplashImage(R.drawable.oneslide, "Welcome to NAME Online!", "A smart way to prepare for Entrance Exam."),
            SplashImage(R.drawable.twoslide, "Multiple Choice Based", "Large volume of questions that are continuously updated."),
            SplashImage(R.drawable.threeslide, "Train Smart, Learn Fast", "Review your performance and practice accordingly.")
        )
        viewPager.adapter = ViewPagerAdapter(onBoardings)
        TabLayoutMediator(
            tabLayout, viewPager
        ) { tab: TabLayout.Tab?, position: Int -> }.attach()
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    2 -> {
                        btn_continue.text = "Get Started"
                        btn_continue.setOnClickListener {
                            findNavController().navigate(R.id.action_viewPagerFragment_to_homeFragment)
                            onBoardingFinish()
                        }
                    }
                    else -> {
                        btn_continue.text = "Continue"
                        btn_continue.setOnClickListener {
                            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                        }
                    }
                }
            }
        })
    }

    private fun onBoardingFinish() {
        val sharedPref = requireActivity().getSharedPreferences("onBoarding", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean("Finished", true)
        editor.apply()
    }
}