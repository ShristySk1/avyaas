package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.SubjectVideo
import kotlinx.android.synthetic.main.item_video_container.view.*

class VideoAdapter(private val subjectVideos: List<SubjectVideo>) :
    RecyclerView.Adapter<VideoAdapter.SubjectVideoViewHolder>() {
    inner class SubjectVideoViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: SubjectVideo) {
            itemView.apply {
                tv_subjectName.text=item.subjectName
                iv_subjectBackground.setImageResource(item.image!!)
                itemView.setOnClickListener {
                    itemSubjectVideoClick?.let {
                        it(item)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VideoAdapter.SubjectVideoViewHolder {
        return SubjectVideoViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_video_container, parent, false)
        )
    }

    override fun onBindViewHolder(holder: VideoAdapter.SubjectVideoViewHolder, position: Int) {
        val item = subjectVideos[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return subjectVideos.size
    }

    private var itemSubjectVideoClick: ((SubjectVideo) -> Unit)? = null
    fun setSubjectVideoClickListener(listener: ((SubjectVideo) -> Unit)) {
        itemSubjectVideoClick = listener
    }
}