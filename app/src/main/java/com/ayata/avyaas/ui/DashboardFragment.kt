package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Offer
import com.ayata.avyaas.ui.adpater.DashboardTotalAdapter
import com.ayata.avyaas.ui.adpater.NoticeAdapter
import com.ayata.avyaas.ui.adpater.OfferAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_view_pager.tabLayout
import kotlinx.android.synthetic.main.fragment_view_pager.viewPager

class DashboardFragment : Fragment(R.layout.fragment_dashboard) {
    lateinit var navController: NavController
    lateinit var dashboardTotalAdapter: DashboardTotalAdapter
    lateinit var dashboardNoticeAdapter: NoticeAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        val navHostFragment = (parentFragment as NavHostFragment)
        val parent = (navHostFragment.parentFragment) as MainFragment
        parent.toolbarHeader("Dashboard")
        val offers = listOf(
            Offer(
                "Hello, Student!",
                "Welcome to Avyaas.",
                "This is a new digital learning platform for NAME Institute of Medical Science.",
                R.drawable.twoslide
            ),
            Offer(
                "Hello, Student!",
                "Welcome to Avyaas.",
                "This is a new digital learning platform for NAME Institute of Medical Science.",
                R.drawable.twoslide
            ),
            Offer(
                "Hello, Student!",
                "Welcome to Avyaas.",
                "This is a new digital learning platform for NAME Institute of Medical Science.",
                R.drawable.twoslide
            )
        )
        var offerAdapter = OfferAdapter(offers)
        viewPager.adapter = offerAdapter
        TabLayoutMediator(
            tabLayout, viewPager
        ) { tab: TabLayout.Tab?, position: Int -> }.attach()
        offerAdapter.setOfferClickListener { position ->
            when (position) {
                0 -> {
                    navController.navigate(R.id.action_dashboardFragment_to_testFragment)
                }

                1 -> {
                    navController.navigate(R.id.action_dashboardFragment_to_testFragment)
                }

                2 -> {
                }
                else -> {
                }
            }

        }
        dashboardTotalAdapter =
            DashboardTotalAdapter(
                listOf(
                    "Total Attempted",
                    "Total Correct",
                    "Total Wrong",
                    "Total Revised"
                )
            )
        rv_dashboardTotal.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            adapter = dashboardTotalAdapter
        }
        dashboardNoticeAdapter =
            NoticeAdapter(
                listOf(
                    "Notice1",
                    "Notice2",
                    "Notice3"
                )
            )
        rv_notice.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = dashboardNoticeAdapter
        }
    }
}