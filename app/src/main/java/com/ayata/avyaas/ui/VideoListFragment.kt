package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.SubjectVideo
import com.ayata.avyaas.ui.adpater.VideoListAdapter
import kotlinx.android.synthetic.main.fragment_video_list.*


class VideoListFragment : Fragment(R.layout.fragment_video_list) {
    lateinit var videoListAdapter: VideoListAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navHostFragment = (parentFragment as NavHostFragment)
        val parent = (navHostFragment.parentFragment) as MainFragment
        parent.toolbarHeader("All Videos")
        val subjectVideos = listOf(
            SubjectVideo("Physics", null, null),
            SubjectVideo("Botany", null, null),
            SubjectVideo("Chemistry", null, null),
            SubjectVideo("Zoology", null, null)
        )
        videoListAdapter= VideoListAdapter(subjectVideos)
        rv_video_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter =videoListAdapter
        }
        videoListAdapter.setSubjectVideoClickListener {
            findNavController().navigate(R.id.action_videoListFragment_to_videoPlayerFragment)
        }

    }
}