package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import kotlinx.android.synthetic.main.item__dashboard_total.view.*

class DashboardTotalAdapter(private val offers: List<String>) :
    RecyclerView.Adapter<DashboardTotalAdapter.OfferViewHolder>() {
    inner class OfferViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: String) {
            itemView.apply {
                tv_label.text = item
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {
        return OfferViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item__dashboard_total, parent, false)
        )
    }

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) {
        val item = offers[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return offers.size
    }


}