package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.drawerlayout.widget.DrawerLayout.SimpleDrawerListener
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupWithNavController
import androidx.navigation.ui.setupWithNavController
import com.ayata.avyaas.R
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.toolbar.view.*


class MainFragment : Fragment(R.layout.fragment_main) {
    private val END_SCALE = 0.7f
    lateinit var navController: NavController
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        navController = Navigation.findNavController(view)
        val nestedNavHostFragment =
            childFragmentManager.findFragmentById(R.id.dashboardFragmentContainer) as NavHostFragment
        navController = nestedNavHostFragment.navController
        navigationView.bringToFront()
        setUpBottomNavigation()
        setupDrawerLayout()
//        (activity as AppCompatActivity).setSupportActionBar(toolbar.toolbar)

        val appBarConfig = AppBarConfiguration(
            setOf(
                R.id.dashboardFragment,
                R.id.subjectFragment,
                R.id.loginFragment,
                R.id.videoFragment,
                R.id.discussionFragment
            ), drawer_layout
        )
        toolbar.toolbar.setupWithNavController(navController, appBarConfig)
        toolbar.toolbar.shape_layout.setOnClickListener {
            navController.navigate(R.id.action_global_profileFragment)
        }


//        back button also opens drawer here
//        val mDrawerToggle = ActionBarDrawerToggle(
//            requireActivity(),
//            drawer_layout,
//            toolbar.toolbar,
//            R.string.drawer_open,
//            R.string.drawer_close
//        );
//        mDrawerToggle.setDrawerIndicatorEnabled(false);
//
//        mDrawerToggle.setToolbarNavigationClickListener {
//            drawer_layout.openDrawer(GravityCompat.START);
//        }
//        mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    private fun setupDrawerLayout() {
        setupWithNavController(navigationView, navController)
        animateNavigationDrawer()
    }

    private fun setUpBottomNavigation() {
        setupWithNavController(bottomNavigationView, navController)
    }

    fun animateNavigationDrawer() {
        drawer_layout.addDrawerListener(object : SimpleDrawerListener() {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

                // Scale the View based on current slide offset
                val diffScaledOffset: Float =
                    slideOffset * (1 - END_SCALE)
                val offsetScale = 1 - diffScaledOffset
                contentView.setScaleX(offsetScale)
                contentView.setScaleY(offsetScale)

                // Translate the View, accounting for the scaled width
                val xOffset = drawerView.width * slideOffset
                val xOffsetDiff: Float = contentView.getWidth() * diffScaledOffset / 2
                val xTranslation = xOffset - xOffsetDiff
                contentView.setTranslationX(xTranslation)
            }
        })
    }

    fun toolbarHeader(title: String) {
        toolbar.toolbar.tv_title.text = title
    }
}