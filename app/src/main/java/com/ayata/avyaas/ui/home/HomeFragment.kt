package com.ayata.avyaas.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayata.avyaas.MainActivity
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Course
import com.ayata.avyaas.ui.adpater.CourseAdapter
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment(R.layout.fragment_home) {
    private val TAG = "HomeFragment"
    private val courseAdapter by lazy {
        CourseAdapter(
            courses =
            listOf(
                Course(
                    "title",
                    "Welcome to NAME Online!",
                    "5 Subjects Included",
                    R.drawable.oneslide
                ),
                Course(
                    "title",
                    "Welcome to NAME Online!",
                    "5 Subjects Included",
                    R.drawable.twoslide
                ),
                Course(
                    "title",
                    "Welcome to NAME Online!",
                    "5 Subjects Included",
                    R.drawable.threeslide
                )
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as MainActivity).clearFullScreenMode()
        super.onViewCreated(view, savedInstanceState)
        rv_courses.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = courseAdapter
        }
        courseAdapter.setCourseClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_mainFragment)
        }
    }

}