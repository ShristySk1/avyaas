package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayata.avyaas.R
import com.ayata.avyaas.ui.adpater.ExploreAdapter1
import com.ayata.avyaas.ui.adpater.ExploreAdapter2
import kotlinx.android.synthetic.main.fragment_discussion_explore.*

class FragmentDiscussionExplore : Fragment(R.layout.fragment_discussion_explore) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rv_explore1.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            adapter = ExploreAdapter1(listOf("subject1", "subject2", "subject3"))
        }
        rv_explore2.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = ExploreAdapter2(listOf("person1", "person2", "person3"))
        }
    }

}