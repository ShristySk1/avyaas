package com.ayata.avyaas.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.Navigation
import com.ayata.avyaas.R
import kotlinx.android.synthetic.main.fragment_blank2.*


class BlankFragment2 : Fragment(R.layout.fragment_blank2) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener {
            // we can find the outer NavController passing the owning Activity
            // and the id of a view associated to that NavController,
            // for example the NavHostFragment id:
             Navigation.findNavController(requireActivity(),
                 R.id.nav_host_fragment_main
             ).navigate(R.id.homeFragment)
        }
        }
}