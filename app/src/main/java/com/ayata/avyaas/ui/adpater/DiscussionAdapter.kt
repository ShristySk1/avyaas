package com.ayata.avyaas.ui.adpater

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ayata.avyaas.ui.FragmentDiscussionExplore
import com.ayata.avyaas.ui.FragmentDiscussionRecent

class DiscussionAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle, val itemsCount: Int) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return itemsCount
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return FragmentDiscussionExplore()
            1 -> return FragmentDiscussionRecent()
        }
        return FragmentDiscussionExplore()
    }
}