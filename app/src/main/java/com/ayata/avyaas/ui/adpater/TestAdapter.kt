package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Test

class TestAdapter(private val tests: List<Test>) :
    RecyclerView.Adapter<TestAdapter.TestViewHolder>() {
    inner class TestViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: Test) {
            itemView.apply {
                
                }
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestAdapter.TestViewHolder {
        return TestViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_test, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TestAdapter.TestViewHolder, position: Int) {
        val item = tests[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return tests.size
    }

    private var itemTestClick: ((Test) -> Unit)? = null
    fun setTestClickListener(listener: ((Test) -> Unit)) {
        itemTestClick = listener
    }
}