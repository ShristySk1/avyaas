package com.ayata.avyaas.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.SubjectVideo
import com.ayata.avyaas.data.model.Topic
import com.ayata.avyaas.ui.adpater.RecentVideoAdapter
import com.ayata.avyaas.ui.adpater.VideoAdapter
import kotlinx.android.synthetic.main.fragment_video.*


class VideoFragment : Fragment(R.layout.fragment_video) {
    lateinit var videoAdapter: VideoAdapter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navHostFragment = (parentFragment as NavHostFragment)
        val parent = (navHostFragment.parentFragment) as MainFragment
        parent.toolbarHeader("Video")

        val recent = listOf(
            SubjectVideo("Physics",null,R.drawable.video_wallpaper),
            SubjectVideo("Botany",null,R.drawable.zoology),
            SubjectVideo("Chemistry",null,R.drawable.video_wallpaper),
            SubjectVideo("Zoology",null,R.drawable.zoology)
        )
        videoAdapter = VideoAdapter(recent)
        rv_video.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            adapter = videoAdapter
        }


        val subjectVideos = listOf(
            SubjectVideo("Physics", null,null),
            SubjectVideo("Botany", null,null),
            SubjectVideo("Chemistry", null,null),
            SubjectVideo("Zoology", null,null)
        )
        viewPager.adapter = RecentVideoAdapter(subjectVideos)
        iv_arrowRight.setOnClickListener {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
        }
        iv_arrowLeft.setOnClickListener {
            viewPager.currentItem?.let {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
            }
        }
        videoAdapter.setSubjectVideoClickListener {
            findNavController().navigate(R.id.action_videoFragment_to_videoListFragment)
        }
    }
}