package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.SubjectVideo

class RecentVideoAdapter(private val subjectVideos: List<SubjectVideo>) :
    RecyclerView.Adapter<RecentVideoAdapter.SubjectVideoViewHolder>() {
    inner class SubjectVideoViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: SubjectVideo) {
            itemView.apply {

            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecentVideoAdapter.SubjectVideoViewHolder {
        return SubjectVideoViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recent_video, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecentVideoAdapter.SubjectVideoViewHolder, position: Int) {
        val item = subjectVideos[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return subjectVideos.size
    }

    private var itemSubjectVideoClick: ((SubjectVideo) -> Unit)? = null
    fun setSubjectVideoClickListener(listener: ((SubjectVideo) -> Unit)) {
        itemSubjectVideoClick = listener
    }
}