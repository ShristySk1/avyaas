package com.ayata.avyaas.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.navigation.fragment.NavHostFragment
import com.ayata.avyaas.R
import kotlinx.android.synthetic.main.fragment_video_player.*

class VideoPlayerFragment : Fragment(R.layout.fragment_video_player) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navHostFragment = (parentFragment as NavHostFragment)
        val parent = (navHostFragment.parentFragment) as MainFragment
        parent.toolbarHeader("Avyaas")
        val mediaController=MediaController(context)
        videoView.setMediaController(mediaController)
        mediaController.setAnchorView(videoView)
    }
}