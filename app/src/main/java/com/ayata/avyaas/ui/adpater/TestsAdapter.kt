package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Test
import kotlinx.android.synthetic.main.items_test.view.*

class TestsAdapter(private val tests: List<String>) :
    RecyclerView.Adapter<TestsAdapter.TestViewHolder>() {
    inner class TestViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: String) {
            itemView.apply {
                rv_tests_container.apply {
                    layoutManager = GridLayoutManager(itemView.context, 2)
                    setHasFixedSize(true)
                    if (item == "All") {
                        val objects = listOf(
                            Test("12/4/2020", "", "", ""),
                            Test("12/4/2020", "", "", ""),
                            Test("12/4/2020", "", "", "")
                        )
                        adapter = TestAdapter(objects)
                    }
                    if (item == "Saturday") {
                        val objects = listOf(
                            Test("12/12/2020", "", "", "")
                        )
                        adapter = TestAdapter(objects)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsAdapter.TestViewHolder {
        return TestViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.items_test, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TestsAdapter.TestViewHolder, position: Int) {
        val item = tests[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return tests.size
    }

    private var itemTestClick: ((Test) -> Unit)? = null
    fun setTestClickListener(listener: ((Test) -> Unit)) {
        itemTestClick = listener
    }
}