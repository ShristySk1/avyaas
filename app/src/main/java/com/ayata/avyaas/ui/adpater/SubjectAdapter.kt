package com.ayata.avyaas.ui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayata.avyaas.R
import com.ayata.avyaas.data.model.Subject
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_subject.view.*

class SubjectAdapter(private val subjects: List<Subject>) :
    RecyclerView.Adapter<SubjectAdapter.SubjectViewHolder>() {
    inner class SubjectViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun bind(item: Subject) {
            itemView.apply {
                tv_subjectName.text = item.subjectName
                Glide.with(itemView.context).load(item.image).into(tv_subjectIcon)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
        return SubjectViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_subject, parent, false)
        )
    }

    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {
        val item = subjects[position]
        if (item != null) holder.bind(item)
    }

    override fun getItemCount(): Int {
        return subjects.size
    }
}