package com.ayata.avyaas.data.model

data class Course(
    val title: String,
    val description: String,
    val subject: String,
    val image: Int
)