package com.ayata.avyaas.data.model

data class SplashImage(
     val image: Int,
     val title: String,
     val subTitle: String
)