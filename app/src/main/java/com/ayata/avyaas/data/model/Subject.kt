package com.ayata.avyaas.data.model

data class Subject(
    val image: Int,
    val subjectName: String,
    val units: List<Units>?
) {

}

data class Units(
    val unitName: String?,
    val chapter: List<Chapter>?,
    val image: Int
)

data class Chapter(
    val chapterName: String?,
    val publishTime: String?
)