package com.ayata.avyaas.data.api

import androidx.lifecycle.LiveData
import com.ayata.avyaas.data.model.Course
import retrofit2.http.GET

interface ApiService {
    @GET
    fun getCourses(): LiveData<List<Course>>
}