package com.ayata.avyaas.data.model

data class SubjectVideo(
    val subjectName: String,
    val topics: List<Topic>?,
    val image:Int?
) {
}

data class Topic(
    val image: Int?,
    val topic: String?,
    val time: String?,
    val date: String?,
    val topicDescription: String?,
    val rating: Float?,
    val views: Long?
)