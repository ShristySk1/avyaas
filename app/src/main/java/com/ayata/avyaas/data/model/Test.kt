package com.ayata.avyaas.data.model

data class Test(
    val date: String,
    val phase: String,
    val testNo: String,
    val time: String
) {
}