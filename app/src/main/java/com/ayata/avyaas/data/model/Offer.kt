package com.ayata.avyaas.data.model

data class Offer(
    val title: String,
    val subTitle: String,
    val description: String,
    val image: Int
) {
}